variable "ram" {
  type        = number
  default     = 4096
  description = "Megabytes of RAM to allocate to Antlet"
}

variable "cpu" {
  type        = number
  default     = 2
  description = "Number of vCPUs to allocate to Antlet"
}

variable "template" {
  type        = string
  default     = "CentOS-7"
  description = "Antsle template to derive antlet from"
}

variable "apikey_auth" {
  # Example value = "Token ey..."
  type        = string
  description = "Get your Antsle token from the API, then put it in this variable. Remember to prefix 'Token ' to the actual token value."
}
