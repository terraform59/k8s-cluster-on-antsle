resource "antsle_antlets" "k8snode1" {
  dname       = "k8snode1"
  template    = var.template
  ram         = var.ram
  cpu         = var.cpu
  antlet_num  = 11
  zpool_name  = "antlets"
  compression = "lz4"
}

resource "antsle_antlets" "k8snode2" {
  dname       = "k8snode2"
  template    = var.template
  ram         = var.ram
  cpu         = var.cpu
  antlet_num  = 12
  zpool_name  = "antlets"
  compression = "lz4"
}

resource "antsle_antlets" "k8snode3" {
  dname       = "k8snode3"
  template    = var.template
  ram         = var.ram
  cpu         = var.cpu
  antlet_num  = 13
  zpool_name  = "antlets"
  compression = "lz4"
}

resource "antsle_antlets" "k8snode4" {
  dname       = "k8snode4"
  template    = var.template
  ram         = var.ram
  cpu         = var.cpu
  antlet_num  = 14
  zpool_name  = "antlets"
  compression = "lz4"
}
