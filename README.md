## Table of contents

* [Requirements](#requirements)
* [Providers](#providers)
* [Modules](#modules)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [License](#license)
* [Author](#author)

---

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_antsle"></a> [antsle](#requirement\_antsle) | >= 2.2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_antsle"></a> [antsle](#provider\_antsle) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| antsle_antlets.k8snode1 | resource |
| antsle_antlets.k8snode2 | resource |
| antsle_antlets.k8snode3 | resource |
| antsle_antlets.k8snode4 | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_apikey_auth"></a> [apikey\_auth](#input\_apikey\_auth) | Get your Antsle token from the API, then put it in this variable. Remember to prefix 'Token ' to the actual token value. | `string` | n/a | yes |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | Number of vCPUs to allocate to Antlet | `number` | `2` | no |
| <a name="input_ram"></a> [ram](#input\_ram) | Megabytes of RAM to allocate to Antlet | `number` | `4096` | no |
| <a name="input_template"></a> [template](#input\_template) | Antsle template to derive antlet from | `string` | `"CentOS-7"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## License

[license](LICENSE) (MIT)

## Author

[Brad Stinson](https://www.bradthebuilder.me/)

[Back to table of contents](#table-of-contents)
