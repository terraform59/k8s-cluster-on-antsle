terraform {
  required_providers {
    antsle = {
      source  = "localhost/antsle/antsle"
      version = ">= 2.2.0"
    }
  }
}

provider "antsle" {
  api_key = var.apikey_auth
}
